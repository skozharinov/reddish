/**
 * @file
 * @author Sergey Kozharinov
 * @date 14 Aug 2020
 * @brief State class
 */

#ifndef REDDISH_INCLUDE_STATE_HPP
#define REDDISH_INCLUDE_STATE_HPP

#include <array>
#include <cstdint>

#include <ArduinoJson.h>

#include "Pin.hpp"

/**
 * @brief Reddish's namespace
 */
namespace Reddish {
/**
 * Represents LED's state in one exact moment of time
 * @tparam N Number of pins of the LED
 */
template <std::size_t N> class State {
public:
  /**
   * @brief Represents duty cycle of one pin
   */
  using LevelT = Pin::LevelT;

  /**
   * @brief Represents an array of duty cycles of all the pins
   */
  using LevelArrayT = std::array<LevelT, N>;

  /**
   * @brief Capacity of the JsonDocument for the state
   */
  static constexpr const std::size_t JSON_SIZE = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(N);

  /**
   * @brief Creates an instance of State class
   * @param levels Duty cycles of all the pins
   * @param isCycled Whether or not transitions are cycled
   */
  constexpr State(LevelArrayT levels, bool isCycled)
      : _levels(std::move(levels)), _isCycled(isCycled) {}

  /**
   * @return Duty cycles of all the pins
   */
  [[nodiscard]] constexpr const LevelArrayT &levels() const { return _levels; }

  /**
   * @return Whether or not transitions are cycled
   */
  [[nodiscard]] constexpr bool isCycled() const { return _isCycled; }

  /**
   * @return An instance of JsonDocument for this state
   */
  [[nodiscard]] constexpr StaticJsonDocument<JSON_SIZE> asJson() const {
    decltype(asJson()) json;

    auto levels = json.createNestedArray("levels");

    for (const auto &level : _levels) {
      levels.add(level);
    }

    json["isCycled"] = _isCycled;

    return json;
  }

private:
  /**
   * @brief Duty cycles of all the pins
   */
  LevelArrayT _levels;

  /**
   * @brief Whether or not transitions are cycled
   */
  bool _isCycled;
};
} // namespace Reddish

#endif // REDDISH_INCLUDE_STATE_HPP
