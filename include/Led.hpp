/**
 * @file
 * @author Sergey Kozharinov
 * @date 14 Aug 2020
 * @brief Led class
 */

#ifndef REDDISH_INCLUDE_LED_HPP
#define REDDISH_INCLUDE_LED_HPP

#include <array>
#include <cstdint>
#include <optional>
#include <queue>

#include "Pin.hpp"
#include "State.hpp"
#include "Transition.hpp"

/**
 * @brief Reddish's namespace
 */
namespace Reddish {
/**
 * @brief Controls multi-channel LED or LED strip
 * @tparam N Number of channels of the LED
 */
template <std::size_t N> class Led {
public:
  /**
   * @brief Represents number of the pin
   */
  using PinT = Pin::PinT;

  /**
   * @brief Represents duty cycle of the pin
   */
  using LevelT = Pin::LevelT;

  /**
   * @brief Represents an amount of time
   */
  using TimeT = typename Transition<N>::TimeT;

  /**
   * @brief Represents an action that executes when transition begins
   */
  using TransitionBeginHandlerFn =
      std::function<void(const State<N> &currentState, const Transition<N> &transition)>;

  /**
   * @brief Represents an action that executes when transition ends
   */
  using TransitionEndHandlerFn = std::function<void(const State<N> &currentState)>;

  /**
   * @brief Creates an instance of Led class for given pins
   * @tparam Pins Parameter pack for the numbers of the pins
   * @param pins Arguments list for the numbers of the pins
   */
  template <class... Pins, class = std::enable_if_t<sizeof...(Pins) == N>,
            class = std::enable_if_t<(std::is_convertible<Pins, PinT>{} && ...)>>
  explicit Led(Pins... pins)
      : _pins{Pin(pins)...}, _transitionBeginTime(0), _lastLoopCall(0), _isCycled(false),
        _isPaused(false) {}

  /**
   * @return Current state of the controller
   */
  [[nodiscard]] State<N> currentState() const {
    std::array<LevelT, N> levels;
    std::transform(_pins.cbegin(), _pins.cend(), levels.begin(), std::mem_fn(&Pin::level));
    return {levels, _isCycled};
  }

  /**
   * @return Whether or not the transition queue is cycled
   */
  [[nodiscard]] bool isCycled() const { return _isCycled; }

  /**
   * @brief Initializes the LED
   */
  void init() {
    std::for_each(_pins.begin(), _pins.end(), std::mem_fn(&Pin::init));
    _lastLoopCall = millis();
  }

  /**
   * @brief Actively controls the LED. Must be called in a loop, as a name suggests
   */
  void loop() {
    auto currentTime = millis();
    auto timeDifference = currentTime - _lastLoopCall;
    _lastLoopCall = currentTime;

    if (_isPaused) {
      _transitionBeginTime += timeDifference;
      return;
    }

    if (!_currentTransition.has_value()) {
      if (!_transitions.empty()) {
        _currentTransition = std::move(_transitions.front());
        _transitionBeginState = currentState();
        _transitionBeginTime = currentTime;
        _transitions.pop();

        if (_transitionBeginCallback) {
          _transitionBeginCallback(*_transitionBeginState, *_currentTransition);
        }
      } else {
        return;
      }
    }

    auto timeSinceStart = currentTime - _transitionBeginTime;

    if (_currentTransition->time() > 0) {
      if (_currentTransition->changesLevels()) {
        if (timeSinceStart >= _currentTransition->time()) {
          for (std::size_t i = 0; i < N; ++i) {
            _pins[i].write(_currentTransition->levels()[i]);
          }
        } else if (timeDifference > 0) {
          auto progress = static_cast<LevelT>(timeSinceStart) / _currentTransition->time();

          for (std::size_t i = 0; i < N; ++i) {
            auto beginLevel = _transitionBeginState->levels()[i];
            auto targetLevel = _currentTransition->levels()[i];

            _pins[i].write(interpolate(beginLevel, targetLevel, progress));
          }
        }
      }
    } else if (_currentTransition->changesLevels()) {
      for (std::size_t i = 0; i < N; ++i) {
        _pins[i].write(_currentTransition->levels()[i]);
      }
    }

    if (timeSinceStart >= _currentTransition->time()) {
      if (_currentTransition->changesCycled()) {
        _isCycled = _currentTransition->isCycled();
      } else if (_isCycled) {
        _transitions.push(*_currentTransition);
      }

      if (_transitionEndCallback) {
        _transitionEndCallback(currentState());
      }

      _currentTransition.reset();
      _transitionBeginState.reset();
      _transitionBeginTime = 0;
    }
  }

  /**
   * @brief Adds given transition to the transition queue
   * @param transition
   */
  void addTransition(Transition<N> transition) { _transitions.push(std::move(transition)); }

  /**
   * @return Whether or not to pause transitions
   */
  [[nodiscard]] bool isPaused() const { return _isPaused; }

  /**
   * @brief Pauses all transitions
   */
  void pause() { _isPaused = true; }

  /**
   * @brief Resumes all transitions
   */
  void resume() { _isPaused = false; }

  /**
   * @brief Execute given action when transition begins
   * @param callback An action
   */
  void onTransitionBegin(TransitionBeginHandlerFn callback) { _transitionBeginCallback = callback; }

  /**
   * @brief Execute given action when transition ends
   * @param callback An action
   */
  void onTransitionEnd(TransitionEndHandlerFn callback) { _transitionEndCallback = callback; }

protected:
  /**
   * @brief Linear interpolation
   * @param begin First number
   * @param end Second number
   * @param position Where to estimate the interpolated value, 0 - near the first number, 1 - near
   * the second one
   * @return Interpolated value
   */
  static constexpr LevelT interpolate(LevelT begin, LevelT end, LevelT position) {
    return begin * (1 - position) + end * position;
  }

private:
  /**
   * @brief Numbers of all controlled pins
   */
  std::array<Pin, N> _pins;

  /**
   * @brief Transition queue
   */
  std::queue<Transition<N>> _transitions;

  /**
   * @brief Current transition
   */
  std::optional<Transition<N>> _currentTransition;

  /**
   * @brief What was the state when transition began
   */
  std::optional<State<N>> _transitionBeginState;

  /**
   * @brief When current transition began
   */
  TimeT _transitionBeginTime;

  /**
   * @brief When loop() was called last time
   */
  TimeT _lastLoopCall;

  /**
   * @brief Whether or not transitions are added to the end of the transition queue after finishing
   */
  bool _isCycled;

  /**
   * @brief Whether or not all transitions are paused
   */
  bool _isPaused;

  /**
   * @brief Action to be executed when transition begins
   */
  TransitionBeginHandlerFn _transitionBeginCallback;

  /**
   * @brief Action to be executed when transition ends
   */
  TransitionEndHandlerFn _transitionEndCallback;
};
} // namespace Reddish

#endif // REDDISH_INCLUDE_LED_HPP
