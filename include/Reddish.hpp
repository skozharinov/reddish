/**
 * @file
 * @author Sergey Kozharinov
 * @date 14 Aug 2020
 * @brief Reddish class
 */

#ifndef REDDISH_INCLUDE_REDDISH_HPP
#define REDDISH_INCLUDE_REDDISH_HPP

#include <cstdint>

#include <ArduinoJson.h>

#include "Led.hpp"
#include "Server.hpp"

/**
 * @brief Reddish's namespace
 */
namespace Reddish {
/**
 * @brief Web server for controlling an LED
 * @tparam N Number of channels of the LED
 */
template <std::size_t N> class Reddish {
public:
  /**
   * @brief Creates an instance of Reddish class
   * @tparam Pins Parameter pack for pins, must be exactly N in size
   * @param pins Argument list for pins, must be exactly N in size
   */
  template <class... Pins>
  explicit Reddish(Pins... pins) : _led(pins...), _server(80) {}

  /**
   * @brief Sets up the server. Needs to be run once
   */
  void init() {
    using namespace std::placeholders;

    _led.init();

    _led.addTransition(TransitionBuilder<N>().cycled(true).build());
    _led.addTransition(TransitionBuilder<N>().levels(1.0f, 0.0f, 0.0f).time(4000).build());
    _led.addTransition(TransitionBuilder<N>().levels(0.0f, 1.0f, 0.0f).time(4000).build());
    _led.addTransition(TransitionBuilder<N>().levels(0.0f, 0.0f, 1.0f).time(4000).build());

    _led.onTransitionBegin(std::bind(&Reddish::handleLedTransitionBegin, this, _1, _2));
    _led.onTransitionEnd(std::bind(&Reddish::handleLedTransitionEnd, this, _1));

    _server.onReset(std::bind(&Reddish::handleServerReset, this, _1));
    _server.onPause(std::bind(&Reddish::handleServerPause, this, _1));
    _server.onResume(std::bind(&Reddish::handleServerResume, this, _1));

    _server.init();
  }

  /**
   * @brief Actively controls the server. As a name suggests, needs to be run in a loop
   */
  void loop() { _led.loop(); }

protected:
  void handleLedTransitionBegin(const State<N> &currentState, const Transition<N> &transition) {
    StaticJsonDocument<State<N>::JSON_SIZE + Transition<N>::JSON_SIZE + JSON_OBJECT_SIZE(2)> data;

    data["currentState"] = currentState.asJson();
    data["transition"] = transition.asJson();

    _server.eventAll("transitionBegin", data.template as<JsonObjectConst>());
  }

  void handleLedTransitionEnd(const State<N> &currentState) {
    StaticJsonDocument<State<N>::JSON_SIZE + JSON_OBJECT_SIZE(1)> data;

    data["currentState"] = currentState.asJson();

    _server.eventAll("transitionEnd", data.template as<JsonObjectConst>());
  }

  void handleServerReset(std::uint8_t) {
    ESP.reset();
  }

  void handleServerPause(std::uint8_t) {
    _led.pause();
  }

  void handleServerResume(std::uint8_t) {
    _led.resume();
  }

private:
  /**
   * @brief An LED
   */
  Led<N> _led;

  /**
   * @brief HTTP server
   */
  Server<N> _server;
};
} // namespace Reddish

#endif // REDDISH_INCLUDE_REDDISH_HPP
