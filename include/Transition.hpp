/**
 * @file
 * @author Sergey Kozharinov
 * @date 14 Aug 2020
 * @brief Transition class
 */

#ifndef REDDISH_INCLUDE_TRANSITION_HPP
#define REDDISH_INCLUDE_TRANSITION_HPP

#include <bitset>
#include <cstdint>

#include <ArduinoJson.h>

#include "Pin.hpp"

/**
 * @brief Reddish's namespace
 */
namespace Reddish {
/**
 * @brief Represents the rules for controlling LED
 * @tparam N Number of LED's channels
 */
template <std::size_t N> class Transition {
public:
  /**
   * @brief Represents an amount of time
   */
  using TimeT = std::uint32_t;

  /**
   * @brief Represents duty cycle of one pin
   */
  using LevelT = Pin::LevelT;

  /**
   * @brief Represents an array of duty cycles of all the pins
   */
  using LevelArrayT = std::array<LevelT, N>;

  /**
   * @brief Capacity of the JsonDocument for the transition
   */
  static constexpr const std::size_t JSON_SIZE = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(N);

  /**
   * @brief Builds Transition
   */
  class Builder;

  /**
   * @return Whether or not the transition has duty cycles set
   */
  [[nodiscard]] constexpr bool changesLevels() const { return _flags[FLAG_POSITION_LEVELS]; }

  /**
   * @return Whether or not the transition has queue cycling set
   */
  [[nodiscard]] constexpr bool changesCycled() const { return _flags[FLAG_POSITION_CYCLED]; }

  /**
   * @return Transition's duty cycles
   */
  [[nodiscard]] constexpr const LevelArrayT &levels() const { return _levels; }

  /**
   * @return Transition's queue cycling
   */
  [[nodiscard]] constexpr bool isCycled() const { return _isCycled; }

  /**
   * @return Transition's time
   */
  [[nodiscard]] constexpr TimeT time() const { return _time; }

  /**
   * @return An instance of JsonDocument for this transition
   */
  [[nodiscard]] constexpr StaticJsonDocument<JSON_SIZE> asJson() const {
    decltype(asJson()) json;

    if (changesLevels()) {
      auto levels = json.createNestedArray("levels");

      for (const auto &level : _levels) {
        levels.add(level);
      }
    }

    if (changesCycled()) {
      json["isCycled"] = _isCycled;
    }

    json["time"] = _time;

    return json;
  }

private:
  /**
   * @brief Positions of change flags in a bitset
   */
  enum FlagPositions : std::size_t { FLAG_POSITION_LEVELS = 0, FLAG_POSITION_CYCLED = 1 };

  /**
   * @brief Represents change flags
   */
  using FlagBitsetT = std::bitset<2>;

  /**
   * @brief Creates an instance of Transition class
   * @param levels Duty cycles of all the pins
   * @param isCycled Whether or not transitions are cycled
   * @param flags What is changed by this transition
   * @param time How much time this transition takes
   */
  constexpr Transition(LevelArrayT levels, bool isCycled, FlagBitsetT flags, TimeT time)
      : _levels(std::move(levels)), _isCycled(isCycled), _flags(std::move(flags)), _time(time) {}

  /**
   * @brief Duty cycles of all the pins
   */
  LevelArrayT _levels;

  /**
   * @brief Whether or not transitions are cycled
   */
  bool _isCycled;

  /**
   * @brief What is changed by this transition
   */
  FlagBitsetT _flags;

  /**
   * @brief How much time this transition takes
   */
  TimeT _time;
};

template <std::size_t N> class Transition<N>::Builder {
public:
  /**
   * @brief Sets Transition's duty cycles
   * @tparam Levels Parameter pack for LED's duty cycles
   * @param levels Argument list for LED's duty cycles
   * @return Updated Builder
   */
  template <class... Levels, class = std::enable_if_t<sizeof...(Levels) == N>,
            class = std::enable_if_t<(std::is_convertible<Levels, LevelT>{} && ...)>>
  constexpr Builder levels(Levels... levels) && {
    _levels = {levels...};
    _flags[FLAG_POSITION_LEVELS] = true;
    return *this;
  }

  /**
   * @brief Sets Transition's queue cycling
   * @param isCycled Whether or not queue will be cycled after the transition finishes. Please
   * note that transitions with this option can't be cycled
   * @return Updated Builder
   */
  constexpr Builder cycled(bool isCycled) && {
    _isCycled = isCycled;
    _flags[FLAG_POSITION_CYCLED] = true;
    return *this;
  }

  /**
   * @brief Sets Transition's time
   * @param time Time for LED's transition
   * @return Updated Builder
   */
  constexpr Builder time(TimeT time) && {
    _time = time;
    return *this;
  }

  /**
   * @brief Unsets Transition's duty cycles
   * @return Updated Builder
   */
  constexpr Builder clearLevels() && {
    _levels.fill(LevelT{});
    _flags[FLAG_POSITION_LEVELS] = false;
    return *this;
  }

  /**
   * @brief Unsets Transition's queue cycling
   * @return Updated Builder
   */
  constexpr Builder clearCycled() && {
    _isCycled = bool{};
    _flags[FLAG_POSITION_CYCLED] = false;
    return *this;
  }

  /**
   * @brief Builds the transition
   * @return An instance of Transition class
   */
  constexpr Transition build() && {
    return {std::move(_levels), _isCycled, std::move(_flags), _time};
  }

private:
  /**
   * @brief Duty cycles of all the pins
   */
  LevelArrayT _levels;

  /**
   * @brief Whether or not transitions are cycled
   */
  bool _isCycled;

  /**
   * @brief What is changed by this transition
   */
  FlagBitsetT _flags;

  /**
   * @brief How much time this transition takes
   */
  TimeT _time;
};

template <std::size_t N> using TransitionBuilder = typename Transition<N>::Builder;
} // namespace Reddish

#endif // REDDISH_INCLUDE_TRANSITION_HPP
