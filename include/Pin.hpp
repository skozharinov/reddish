/**
 * @file
 * @author Sergey Kozharinov
 * @date 14 Aug 2020
 * @brief Pin class
 */

#ifndef REDDISH_INCLUDE_PIN_HPP
#define REDDISH_INCLUDE_PIN_HPP

#include <cstdint>

#include <Arduino.h>

/**
 * @brief Reddish's namespace
 */
namespace Reddish {
/**
 * @brief Controls one PWM pin of the board
 */
class Pin {
public:
  /**
   * @brief Represents number of the pin
   */
  using PinT = std::uint8_t;

  /**
   * @brief Represents duty cycle of one pin
   */
  using LevelT = float;

  /**
   * @brief Sets how many bits represent possible values of PWM duty cycle
   * @param resolution PWM resolution, bits
   */
  inline static void setPwmResolution(unsigned int resolution = 10) {
    analogWriteResolution(static_cast<int>(resolution));
    _pwmResolution = resolution;
  };

  /**
   * @brief Sets how fast the PWM completes a cycle
   * @param frequency PWM frequency, Hz
   */
  inline static void setPwmFrequency(unsigned int frequency = 1000) {
    analogWriteFreq(frequency);
    _pwmFrequency = frequency;
  };

  /**
   * @brief Creates an instance of Pin class for given pin
   * @param pin Number of the PWM pin
   */
  inline explicit Pin(PinT pin) : _pin(pin), _level(0) {}

  /**
   * @return Number of the PWM pin
   */
  [[nodiscard]] inline PinT pin() const { return _pin; }

  /**
   * @return Current duty cycle of the PWM, from 0 to 1 inclusive
   */
  [[nodiscard]] inline LevelT level() const { return _level; }

  /**
   * @brief Initializes PWM
   */
  inline void init() { write(_level); }

  /**
   * @brief Generates PWM signal on the pin with given duty cycle
   * @param level New duty cycle of the PWM, from 0 to 1 inclusive
   */
  inline void write(LevelT level) {
    if (_pwmResolution == 0)
      setPwmResolution();
    if (_pwmFrequency == 0)
      setPwmFrequency();

    _level = level;
    analogWrite(_pin, static_cast<int>(_level * static_cast<float>((1u << _pwmResolution) - 1)));
  }

private:
  /**
   * @brief PWM resolution, bits
   */
  inline static unsigned int _pwmResolution = 0;

  /**
   * @brief PWM frequency, Hz
   */
  inline static unsigned int _pwmFrequency = 0;

  /**
   * @brief Number of the PWM pin
   */
  PinT _pin;

  /**
   * @brief Duty cycle of the PWM, from 0 to 1 inclusive
   */
  LevelT _level;
};
} // namespace Reddish

#endif // REDDISH_INCLUDE_PIN_HPP
