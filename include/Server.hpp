/**
 * @file
 * @author Sergey Kozharinov
 * @date 14 Aug 2020
 * @brief Server class
 */

#ifndef REDDISH_INCLUDE_SERVER_HPP
#define REDDISH_INCLUDE_SERVER_HPP

#define WEBSERVER_H

#include <cstdint>

#include <ArduinoJson.h>
#include <ESPAsyncWebServer.h>
#include <LittleFS.h>

/**
 * @brief Reddish's namespace
 */
namespace Reddish {
/**
 * @brief Web app and API
 * @tparam N Number of the LED's pins
 */
template <std::size_t N> class Server : protected AsyncWebServer {
public:
  /**
   * @brief Represents an action that is executed when reset command is received
   */
  using ResetHandlerFn = std::function<void(std::uint8_t clientId)>;

  /**
   * @brief Represents an action that is executed when pause command is received
   */
  using PauseHandlerFn = std::function<void(std::uint8_t clientId)>;

  /**
   * @brief Represents an action that is executed when resume command is received
   */
  using ResumeHandlerFn = std::function<void(std::uint8_t clientId)>;

  /**
   * @brief Creates an instance of Server class
   * @param port Port of the server
   */
  explicit Server(std::uint16_t port) : AsyncWebServer(port), _ws("/api") {}

  /**
   * @brief Initializes the server
   */
  void init() {
    using namespace std::placeholders;

    _ws.onEvent(std::bind(&Server::handleWsEvent, this, _1, _2, _3, _4, _5, _6));
    onNotFound(std::bind(&Server::handleNotFound, this, _1));
    addHandler(&_ws);

    LittleFS.begin();
    auto &staticHandler = serveStatic("/", LittleFS, "/www");
    staticHandler.setDefaultFile("index.html");

    begin();
  }

  /**
   * @brief Execute given action when reset command is received
   * @param callback An action
   */
  void onReset(ResetHandlerFn callback) { _resetCallback = callback; }

  /**
   * @brief Execute given action when pause command is received
   * @param callback An action
   */
  void onPause(PauseHandlerFn callback) { _pauseCallback = callback; }

  /**
   * @brief Execute given action when resume command is received
   * @param callback An action
   */
  void onResume(ResumeHandlerFn callback) { _resumeCallback = callback; }

  /**
   * @brief Broadcasts given event to all connected clients
   * @param type Type of the event
   * @param data Data to be broadcast
   */
  void eventAll(const char *type, JsonObjectConst data) {
    DynamicJsonDocument message(data.memoryUsage() + JSON_OBJECT_SIZE(2) +
                                JSON_STRING_SIZE(std::strlen(type)));

    message["type"] = type;
    message["data"] = data;

    auto messageSize = measureJson(message);
    auto buffer = new char[messageSize + 1];

    serializeJson(message, buffer, messageSize + 1);

    _ws.textAll(buffer, messageSize);

    delete[] buffer;
  }

protected:
  void handleNotFound(AsyncWebServerRequest *request) { request->send(404); }

  /**
   * @brief Handles WebSocket events
   * @param server The server
   * @param client The client
   * @param type Type of the event
   * @param arg
   * @param data Received data
   * @param length Length of the data
   */
  void handleWsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type,
                     void *arg, std::uint8_t *data, std::size_t length) {
    switch (type) {
    case WS_EVT_CONNECT:
      break;
    case WS_EVT_DISCONNECT:
      break;
    case WS_EVT_PONG:
      break;
    case WS_EVT_ERROR:
      break;
    case WS_EVT_DATA:
      handleWsData(client->id(), arg, data, length);
    }
  }

  /**
   * @brief Handles WebSocket data
   * @param clientId ID of the client
   * @param arg
   * @param data Received data
   * @param length Length of the data
   */
  void handleWsData(std::uint32_t clientId, void *arg, std::uint8_t *data, std::size_t length) {
    StaticJsonDocument<1000> messageJson;
    auto error = deserializeJson(messageJson, data, length);

    if (error)
      return;

    auto type = messageJson["type"].as<char *>();

    if (std::strcmp(type, "reset") == 0) {
      if (_resetCallback)
        _resetCallback(clientId);
    } else if (std::strcmp(type, "pause") == 0) {
      if (_pauseCallback)
        _pauseCallback(clientId);
    } else if (std::strcmp(type, "resume") == 0) {
      if (_resumeCallback)
        _resumeCallback(clientId);
    }
  }

private:
  /**
   * @brief WebSocket endpoint
   */
  AsyncWebSocket _ws;

  /**
   * @brief An action that is executed when reset command is received
   */
  ResetHandlerFn _resetCallback;

  /**
   * @brief An action that is executed when pause command is received
   */

  PauseHandlerFn _pauseCallback;

  /**
   * @brief An action that is executed when resume command is received
   */
  ResetHandlerFn _resumeCallback;
};
} // namespace Reddish

#endif // REDDISH_INCLUDE_SERVER_HPP
