#include <WString.h>
#include <ArduinoOTA.h>
#include <WiFiManager.h>
#include <ESP8266WiFiMulti.h>

#include "Reddish.hpp"

const std::uint8_t RED_PIN = 14;
const std::uint8_t GREEN_PIN = 12;
const std::uint8_t BLUE_PIN = 13;

static Reddish::Reddish<3> reddish(RED_PIN, GREEN_PIN, BLUE_PIN);

void setup() {
  auto name = ("Reddish-" + String(ESP.getChipId()));
  auto password = "kukuruzen";

  WiFiManager wifiManager;
  wifiManager.setDebugOutput(false);
  wifiManager.autoConnect(name.c_str(), password);

  ArduinoOTA.setPort(8266);
  ArduinoOTA.setHostname(name.c_str());
  ArduinoOTA.setPassword(password);
  ArduinoOTA.begin();

  reddish.init();
}

void loop() {
  ArduinoOTA.handle();
  reddish.loop();
}

