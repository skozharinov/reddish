let ws;

function connectWebSocket() {
  ws = new WebSocket(`ws://${window.location.hostname}/api`);

  ws.addEventListener("open", () => {
    createMessage("Connected to your LED");
  });

  ws.addEventListener("close", () => {
    createMessage("Connection closed, reconnecting in 1 second");
    setTimeout(connectWebSocket, 1000);
  });

  ws.addEventListener("message", (message) => {
    handleData(JSON.parse(message.data));
  });

  ws.addEventListener("error", () => {
    createErrorMessage("Error while connecting to LED");
    ws.close();
  })

  createMessage("Connecting to your LED...");
}

function handleData(data) {
  if (typeof handleData.transitionUpdateLevels === "undefined") handleData.transitionUpdateLevels = null;

  if (data["type"] === "transitionBegin") {
    let levelsTableBody = document.querySelector("#levels__table tbody");

    let currentLevels = data["data"]["currentState"]["levels"];
    let targetLevels = data["data"]["transition"]["levels"];

    let transitionBeginTime = Date.now();
    let transitionDuration = data["data"]["transition"]["time"];

    if (handleData.transitionUpdateLevels !== null) {
      clearInterval(handleData.transitionUpdateLevels);
      handleData.transitionUpdateLevels = null;
    }

    handleData.transitionUpdateLevels = setInterval(() => {
      let currentTime = Date.now();
      let elapsedTime = currentTime - transitionBeginTime;

      if (elapsedTime > transitionDuration) {
        clearInterval(handleData.transitionUpdateLevels);
        handleData.transitionUpdateLevels = null;

        setLevels(levelsTableBody, targetLevels);
        return;
      }

      let progress = elapsedTime / transitionDuration;

      let nextLevels = currentLevels
          .map((value, index) => [value, targetLevels[index]])
          .map(([current, target]) => interpolate(current, target, progress));

      setLevels(levelsTableBody, nextLevels);
    }, 50);
  } else if (data["type"] === "transitionEnd") {
    let levels = data["data"]["currentState"]["levels"];
    let levelsTableBody = document.querySelector("#levels__table tbody");

    if (handleData.transitionUpdateLevels !== null) {
      clearInterval(handleData.transitionUpdateLevels);
      handleData.transitionUpdateLevels = null;
    }

    setLevels(levelsTableBody, levels);
  }
}

/**
 * Ensures that given table has the exact needed number of rows
 * @param {Element} levelsTableBody Table to be modified
 * @param {number} numberOfChannels Number of the LED's channels
 */
function fillLevelsTable(levelsTableBody, numberOfChannels) {
  if (levelsTableBody.childNodes.length > numberOfChannels) {
    while (levelsTableBody.childNodes.length > numberOfChannels) {
      levelsTableBody.removeChild(levelsTableBody.lastChild);
    }
  } else if (levelsTableBody.childNodes.length < numberOfChannels) {
    for (let i = levelsTableBody.childNodes.length; i < numberOfChannels; ++i) {
      let row = document.createElement("tr");

      let channel = document.createElement("td");
      channel.textContent = `${i + 1}`;

      let level = document.createElement("td");
      level.textContent = "- %";

      let levelBar = document.createElement("progress");

      let levelBarTd = document.createElement("td");
      levelBarTd.appendChild(levelBar);

      row.appendChild(channel);
      row.appendChild(level);
      row.appendChild(levelBarTd);

      levelsTableBody.appendChild(row);
    }
  }
}

/**
 * Linear interpolation
 * @param {number} begin First number
 * @param {number} end Second number
 * @param {number} position Where to estimate the interpolated value, 0 - near the first number, 1 - near
 * the second one
 * @return {number} Interpolated value
 */
function interpolate(begin, end, position) {
  return begin * (1 - position) + end * position;
}

/**
 * Fills given table with level values
 * @param levelsTableBody Table to be modified
 * @param levels Duty cycles of all the pins of the LED
 */
function setLevels(levelsTableBody, levels) {
  fillLevelsTable(levelsTableBody, levels.length);

  for (let i = 0; i < levels.length; ++i) {
    let row = levelsTableBody.querySelector(`tr:nth-child(${i + 1})`);

    let level = row.querySelector(`td:nth-child(2)`);
    let levelBar = row.querySelector(`td:nth-child(3) progress`);

    level.textContent = `${Math.round(levels[i] * 100)} %`;

    levelBar.setAttribute("value", levels[i]);
  }
}

/**
 * Shows a message in the message container
 * @param {string} message Message to be shown
 */
function createMessage(message) {
  let msg = document.createElement("div");
  msg.classList.add("message");

  let msgP = document.createElement("p");
  msgP.classList.add("message-text");
  msgP.textContent = message;

  msg.appendChild(msgP);

  _showMessage(msg);
}

/**
 * Shows an error message in the message container
 * @param {string} message Message to be shown
 */
function createErrorMessage(message) {
  let msg = document.createElement("div");
  msg.classList.add("message");
  msg.classList.add("error-message");

  let msgP = document.createElement("p");
  msgP.classList.add("message-text");
  msgP.textContent = message;

  msg.appendChild(msgP);

  _showMessage(msg);
}

/**
 * Places given message element in the message container
 * @param {Element} messageElement Message element
 * @throws {Error} When given element doesn't have message class
 * @private
 */
function _showMessage(messageElement) {
  if (!messageElement.classList.contains("message")) throw new Error("Cannot show message without message class");

  let msgContainer = document.querySelector(".message-container");

  msgContainer.appendChild(messageElement);

  setTimeout(() => {
    if (window.matchMedia("(prefers-reduced-motion: no-preference)").matches) {
      messageElement.classList.add("message-removed");

      messageElement.addEventListener("animationend", () => {
        msgContainer.removeChild(messageElement);
      });
    } else {
      msgContainer.removeChild(messageElement);
    }
  }, 2000);
}

/**
 * Send command to reset the module
 */
function reset() {
  ws.send(JSON.stringify({"type": "reset"}));

  createMessage("Successfully rebooted the controller");
}

/**
 * Send command to pause all transitions
 */
function pause() {
  ws.send(JSON.stringify({"type": "pause"}));

  createMessage("Successfully paused the controller");
}

/**
 * Send command to resume transitions
 */
function resume() {
  ws.send(JSON.stringify({"type": "resume"}));

  createMessage("Successfully resumed the controller");
}

window.onload = connectWebSocket;
